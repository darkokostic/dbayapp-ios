import {Injectable} from "@angular/core";
import {Http, ConnectionBackend, RequestOptions, RequestOptionsArgs, Response, Headers} from "@angular/http";
import {Observable} from "rxjs";
import {RestResponse} from "../shared/restResponse";
import {Constants} from "../shared/constants";
import { LocalStorageService } from 'angular-2-local-storage';

@Injectable()
export class HttpInterceptor extends Http {
    constructor(backend: ConnectionBackend, defaultOptions: RequestOptions, public localStorage: LocalStorageService) {
        super(backend, defaultOptions);
    }

    get(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return super.get(url, this.getAuthTokenHeader(options))
            .do(this.successfulRequest,
                (err: Response) => {
                    return this.handleError(err);
                });
    }

    post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
        return super.post(url, body, this.getAuthTokenHeader(options))
            .do(this.successfulRequest,
                (err: Response) => {
                    return this.handleError(err);
                });
    }

    delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
        return super.delete(url, this.getAuthTokenHeader(options))
            .do(this.successfulRequest,
                (err: Response) => {
                    return this.handleError(err);
                });
    }

    put(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
        return super.put(url, body, this.getAuthTokenHeader(options))
            .do(this.successfulRequest,
                (err: Response) => {
                    return this.handleError(err);
                });
    }

    private handleError (err: Response | any) {
        switch (err.status) {
            case 401:
                err._body = new RestResponse();
                err._body.message = Constants.SESSION_EXPIRED_MESSAGE;
                err._body.statusCode = err.status;
                break;
            case 403:
                err._body = new RestResponse();
                err._body.message = Constants.FORBIDDEN_MESSAGE;
                err._body.statusCode = err.status;
                break;
            case 413:
                err._body = new RestResponse();
                err._body.message = Constants.IMAGE_TOO_LARGE_MESSAGE;
                err._body.statusCode = err.status;
                break;
            case 500:
                err._body = new RestResponse();
                err._body.message = Constants.SERVER_ERROR_MESSAGE;
                err._body.statusCode = err.status;
                break;
            case 0:
                err._body = new RestResponse();
                err._body.message = Constants.NO_INTERNET_CONNECTION_MESSAGE;
                err._body.statusCode = err.status;
                break;
        }
        return Observable.throw(err);
    }

    private getAuthTokenHeader(options: RequestOptionsArgs): RequestOptions {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append(Constants.TOKEN_KEY, Constants.TOKEN_VALUE);

        if(options && options.search) {
            return new RequestOptions({ headers: headers, search: options.search });
        } else {
            return new RequestOptions({ headers: headers });
        }
    }

    private successfulRequest(res: Response) {
        return Observable;
    }
}