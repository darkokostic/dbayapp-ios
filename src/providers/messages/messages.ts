import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from "rxjs";
import {Constants} from "../../shared/constants";
import {LocalStorageService} from "angular-2-local-storage";

@Injectable()
export class MessagesProvider {

    constructor(public http: Http, public localStorage: LocalStorageService) {}

    getUserMessages(): Observable<any> {
        let userInfo: any = this.localStorage.get(Constants.USER_INFO_LOCALSTORAGE);
        return this.http.get(Constants.USER_MESSAGES_API + userInfo.id)
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }

    getSingleMessage(id): Observable<any> {
        return this.http.get(Constants.SINGLE_MESSAGE_API + id)
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }

    sendMessage(messageObject: any, caseId: number): Observable<any> {
        console.log(messageObject);
        console.log(caseId);
        let userInfo: any = this.localStorage.get(Constants.USER_INFO_LOCALSTORAGE);
        return this.http.post(Constants.SINGLE_MESSAGE_API + userInfo.id + "/case/" + caseId, JSON.stringify(messageObject))
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }
}
