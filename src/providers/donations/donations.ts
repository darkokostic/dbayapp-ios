import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs';
import {Constants} from "../../shared/constants";
import {LocalStorageService} from "angular-2-local-storage";

@Injectable()
export class DonationsProvider {

    constructor(public http: Http, public localStorage: LocalStorageService) {}

    getDonations(): Observable<any> {
        return this.http.get(Constants.DONATIONS_API)
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }

    getSingleDonation(id: number): Observable<any> {
        return this.http.get(Constants.DONATIONS_API + "/" + id)
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }

    getMyDonations(page: number): Observable<any> {
        let userInfo: any = this.localStorage.get(Constants.USER_INFO_LOCALSTORAGE);
        return this.http.get(Constants.USER_DONATIONS_API + userInfo.id + "?page=" + page)
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }

    postDonation(donationObject: any): Observable<any> {
        console.log(donationObject);
        return this.http.post(Constants.DONATIONS_API, JSON.stringify(donationObject))
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }

    editDonation(donationObject: any, id: number): Observable<any> {
        return this.http.post(Constants.DONATIONS_API + "/" + id, JSON.stringify(donationObject))
            .map((res: Response) => res.json())
            .catch((err:any) => {
                let details = err.json();
                return Observable.throw(details);
            })
    }

}
