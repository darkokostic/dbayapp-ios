import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SwipeProvider {

    constructor(public http: Http) {}

    swipe(navCtrl: any): void {
        if(navCtrl.canGoBack()) {
            navCtrl.pop();
        }
    }
}
