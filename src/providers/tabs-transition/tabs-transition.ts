import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class TabsTransitionProvider {
    private hasGoToTab: any = null;

    constructor(public http: Http) {
    }

    setTab(page: any): void {
        this.hasGoToTab = page;
    }

    getTab(): any {
        return this.hasGoToTab;
    }

}
