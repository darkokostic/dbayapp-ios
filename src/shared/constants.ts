import { Injectable } from '@angular/core';

@Injectable()
export class Constants {
    public static ENDPOINT: string = "http://dbay.cyan.boutique/mobile_api/";
    public static IMAGES_ENDPOINT: string = "http://dbay.cyan.boutique/uploaded_files/";
    public static ABOUT_API: string = Constants.ENDPOINT + "page/2";
    public static LOGIN_API: string = Constants.ENDPOINT + "login";
    public static REGISTER_API: string = Constants.ENDPOINT + "register";
    public static RESET_PASSWORD_API: string = Constants.ENDPOINT + "reset_password";
    public static FAQ_API: string = Constants.ENDPOINT + "page/3";
    public static TERMS_API: string = Constants.ENDPOINT + "page/1";
    public static CASES_API: string = Constants.ENDPOINT + "cases";
    public static CATEGORY_CASES_API: string = Constants.ENDPOINT + "cases/cat/";
    public static USER_CASES_API: string = Constants.ENDPOINT + "cases/user/";
    public static CLOSE_CASE_API: string = Constants.ENDPOINT + "cases/close/";
    public static DONATIONS_API: string = Constants.ENDPOINT + "donations";
    public static USER_DONATIONS_API: string = Constants.ENDPOINT + "donations/user/";
    public static CITIES_API: string = Constants.ENDPOINT + "cities";
    public static USER_MESSAGES_API: string = Constants.ENDPOINT + "messages/user/";
    public static SINGLE_MESSAGE_API: string = Constants.ENDPOINT + "messages/";
    public static UPDATE_PROFILE_API: string = Constants.ENDPOINT + "settings/profile/";
    public static CHANGE_PASSWORD_API: string = Constants.ENDPOINT + "settings/password/";
    public static DEACTIVATE_ACC_API: string = Constants.ENDPOINT + "settings/deactivate/";
    public static CATEGORIES_API: string = Constants.ENDPOINT + "categories";
    public static ATTACHMENT_API: string = Constants.ENDPOINT + "attachment";
    public static RULES_API: string = Constants.ENDPOINT + "rules";
    public static CONTACT_API: string = Constants.ENDPOINT + "contact/send";

    // Interceptor Messages
    public static SESSION_EXPIRED_MESSAGE: string = "لقد انتهت مدة الجلسة . برجاء اعادة تسجيل الدخول مرة اخري";
    public static FORBIDDEN_MESSAGE: string = "Forbidden";
    public static NOT_FOUND_MESSAGE: string = "Not Found";
    public static NO_INTERNET_CONNECTION_MESSAGE: string = "برجاء فحص اتصال الانترنت";
    public static IMAGE_TOO_LARGE_MESSAGE: string = "فشل في تحميل الملف , حجم الملف كبير جدا";
    public static SERVER_ERROR_MESSAGE: string = "هناك مشكلة في الاتصال بالسيرفر , برجاء المحاولة مرة اخري";

    // Registration Messages
    public static FILL_ALL_FIELDS_MESSAGE: string = "برجاء مليء جميع الحقول المطلوبة";
    public static PASSWORD_LENGTH_MESSAGE: string = "الرقم السري يجب ان يكون ٧ أحرف على الاقل";
    public static PASSWORDS_MUST_MATCH_MESSAGE: string = "كلمات السر يجب ان تتطابق";
    public static EMAIL_FIELD_INVALID: string = "لقد ادخلت بريد الكتروني غير صحيح";
    public static REGISTER_MESSAGE: string = "تمت عملية التسجيل بنجاح";
    public static RULES_MESSAGE: string = "يجب عليك الموافقة على الشروط والقوانين لأتمام عملية التسجيل";

    // Login Messages
    public static LOGIN_MESSAGE: string = "تم تسجيل الدخول بنجاح";
    public static FAILD_LOGIN_MESSAGE: string = "برجاء مليء اسم المستخدم وكلمة المرور !";

    // storage keys
    public static TOKEN_KEY: string = "_token";
    public static TOKEN_VALUE: string = "token";
    public static USER_INFO_LOCALSTORAGE: string = "user_info";
    public static CATEGORY_LOCALSTORAGE: string = "category";
    public static ONESIGNAL_ID_KEY: string = "onesignalID";

    // loading messages
    public static PLEASE_WAIT_MESSAGE: string = "برجاء الانتظار ...";
    public static LOADING_DATA_MESSAGE: string = "جاري تحميل البيانات ...";

    // User Profile Settings
    public static UPDATED_USER_PROFILE_MESSAGE: string = "تم تغير اعدادات الملف الشخصي بنجاح !";
    public static CHANGED_PASSWORD_MESSAGE: string = "تم تغير كلمة السر بنجاح !";
    public static AGREE_DEACTIVATE_PROFILE_MESSAGE: string = "يجب عليك الموافقة اولا على تعطيل الحساب من خلال زر التعطيل";

    // Donations
    public static CREATED_DONATION_MESSAGE: string = "لقد تمت المساهمة بنجاح";

    // Cases
    public static CREATED_CASE_MESSAGE: string = "تم انشاء الحالة بنجاح";
    public static EDITED_CASE_MESSAGE: string = "تم تعديل الحالة بنجاح";
    public static SENT_NEW_MESSAGE: string = "تم ارسال الرسالة بنجاح";
    public static CLOSED_CASE_MESSAGE: string = "تم اغلاق الحالة";

    // Message for empty arrey
    public static MY_DONATIONS_MESSAGE: string = "لا يوجد لديك اي مساهمات حتي الأن";
    public static MY_CASES_MESSAGE: string = "لا يوجد لديك اي حالات حتي الأن";
    public static MY_MESSAGES_MESSAGE: string = "لا يوجد لديك اي رسائل مرسلة حتي ألأن";

    // Reset password messages
    public static FILL_EMAIL_FIELD: string = "برجاء مليء حقل البريد الالكتروني !";

    // Upload image action sheet
    public static TAKE_A_PICTURE_LABEL: string = "التقاط صورة";
    public static CHOOSE_FROM_LIBRARY_LABEL: string = "اختر صورة من مكتبتي";
    public static CANCEL_LABEL: string = "الغاء";

    // Delete image confirmation popup
    public static DELETE_IMAGE_TITLE: string = "Delete image";
    public static DELETE_IMAGE_QUESTION: string = "Are you sure that you want to delete image?";
    public static DELETE_IMAGE_CANCEL_LABEL: string = "الغاء";
    public static DELETE_IMAGE_YES_LABEL: string = "Yes";

    // Deactivate acc message
    public static DEACTIVATE_ACC: string = "تم تعطيل حسابك";

    constructor() {}
}