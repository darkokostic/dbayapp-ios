import {DonatePage} from './../donate/donate';
import {FileOpener} from '@ionic-native/file-opener';
import {Component,} from '@angular/core';
import {NavController, NavParams, LoadingController} from 'ionic-angular';
import {CasesProvider} from "../../providers/cases/cases";
import {Constants} from "../../shared/constants";
import {LocalStorageService} from "angular-2-local-storage";
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {MessagesProvider} from "../../providers/messages/messages";
import {Toast} from "@ionic-native/toast";
import {SwipeProvider} from "../../providers/swipe/swipe";

@Component({
    selector: 'page-case',
    templateUrl: 'case.html',
})

export class CasePage {
    public id: number;
    private singleCase: any = {
        id: 0,
        title: "",
        details: "",
        city_name: null,
        category_name: null,
        address_area: "",
        attachments: []
    };
    private catCases: any = [];
    private userInfo: any;
    private area: any;
    private city: any;
    private endpoint: string;
    private canSendMessage: boolean = false;
    private form: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, private fileOpener: FileOpener, private casesProvider: CasesProvider, public localStorage: LocalStorageService, private messagesProvider: MessagesProvider, public loadingCtrl: LoadingController, private toast: Toast, public swipeProvider: SwipeProvider) {
        this.endpoint = Constants.IMAGES_ENDPOINT;
        this.id = this.navParams.get('id');
        this.userInfo = this.localStorage.get(Constants.USER_INFO_LOCALSTORAGE);
        this.form = new FormGroup({
            subject: new FormControl(this.id + ' رسالة بخصوص الحالة رقم', Validators.required),
            content: new FormControl('', Validators.required),
        });
    }

    goToCase(id) {
        this.navCtrl.push(CasePage, {id: id});
    }


    donate() {
        this.navCtrl.push(DonatePage, this.singleCase);
    }

    openImg(imgUrl: string) {
        this.fileOpener.open(imgUrl, 'image/jpeg')
            .then(() => console.log('File is opened'))
            .catch(e => console.log('Error openening file', e));
    }

    ionViewDidEnter() {
        let loader = this.loadingCtrl.create({
            content: Constants.LOADING_DATA_MESSAGE,
        });
        loader.present();
        this.casesProvider.getSingleCase(this.id)
            .subscribe(
                (response: any) => {
                    this.singleCase = response[0];
                    this.area = this.userInfo.area;
                    this.city = this.userInfo.city;
                    this.casesProvider.getCatCases(this.singleCase.category, 1)
                        .subscribe(
                            (response: any) => {
                                this.catCases = response.data;
                                loader.dismiss();
                            }, (error: any) => {
                                loader.dismiss();
                                if(error.msg) {
                                    this.toast.show(error.msg, '2000', 'center')
                                        .subscribe(
                                            toast => {
                                                console.log(toast);
                                            }
                                        );
                                } else if(error.message) {
                                    this.toast.show(error.message, '2000', 'center')
                                        .subscribe(
                                            toast => {
                                                console.log(toast);
                                            }
                                        );
                                } else {
                                    for(let key in error){
                                        error[key].forEach(message => {
                                            this.toast.show(message, '2000', 'center')
                                                .subscribe(
                                                    toast => {
                                                        console.log(toast);
                                                    }
                                                );
                                        });
                                    }
                                }
                            }
                        );
                }, (error: any) => {
                    loader.dismiss();
                    if(error.msg) {
                        this.toast.show(error.msg, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else if(error.message) {
                        this.toast.show(error.message, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else {
                        for(let key in error){
                            error[key].forEach(message => {
                                this.toast.show(message, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            });
                        }
                    }
                }
            );
    }

    sendMessage(): void {
        if(this.form.valid) {
            let loader = this.loadingCtrl.create({
                content: Constants.PLEASE_WAIT_MESSAGE,
            });
            loader.present();
            this.messagesProvider.sendMessage(this.form.value, this.id)
                .subscribe(
                    (response: any) => {
                        loader.dismiss();
                        this.canSendMessage = false;
                        this.form.controls['subject'].setValue("");
                        this.form.controls['content'].setValue("");
                        this.toast.show(Constants.SENT_NEW_MESSAGE, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    }, (error: any) => {
                        loader.dismiss();
                        if(error.msg) {
                            this.toast.show(error.msg, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        } else if(error.message) {
                            this.toast.show(error.message, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        } else {
                            for(let key in error){
                                error[key].forEach(message => {
                                    this.toast.show(message, '2000', 'center')
                                        .subscribe(
                                            toast => {
                                                console.log(toast);
                                            }
                                        );
                                });
                            }
                        }
                    }
                );
        }
    }

    swipeEvent(e: any):void {
        if(e.direction == 4) {
            this.swipeProvider.swipe(this.navCtrl);
        }
    }
}
