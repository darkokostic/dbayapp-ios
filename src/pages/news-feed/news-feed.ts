import {CasePage} from './../case/case';
import {Component} from '@angular/core';
import {NavController, NavParams, LoadingController} from 'ionic-angular';
import {CasesProvider} from "../../providers/cases/cases";
import {CategoriesProvider} from "../../providers/categories/categories";
import {Constants} from "../../shared/constants";
import {LocalStorageService} from "angular-2-local-storage";
import {Toast} from "@ionic-native/toast";
import {SwipeProvider} from "../../providers/swipe/swipe";

@Component({
    selector: 'page-news-feed',
    templateUrl: 'news-feed.html',
})
export class NewsFeedPage {

    private allCases: any = [];
    private categories: any = [];
    private choosedCategory: any;
    private userInfo: any;
    private area: any;
    private city: any;
    private page: number;
    private hasMore: boolean;

    constructor(public navCtrl: NavController, public navParams: NavParams, public swipeProvider: SwipeProvider, private casesProvider: CasesProvider, private categoriesProvider: CategoriesProvider, public localStorage: LocalStorageService, public loadingCtrl: LoadingController, private toast: Toast) {
        this.page = 1;
        this.hasMore = true;
        this.choosedCategory = null;
        this.userInfo = this.localStorage.get(Constants.USER_INFO_LOCALSTORAGE);
        this.area = this.userInfo.area;
        this.city = this.userInfo.city;
    }

    goToCase(id) {
        this.navCtrl.push(CasePage, {id: id});
    }

    ionViewDidEnter() {
        let loader = this.loadingCtrl.create({
            content: Constants.LOADING_DATA_MESSAGE,
        });
        loader.present();
        this.categoriesProvider.getCategories()
            .subscribe(
                (response: any) => {
                    this.categories = response;
                    this.getAllCases(loader, null);
                }, (error: any) => {
                    loader.dismiss();
                    if(error.msg) {
                        this.toast.show(error.msg, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else if(error.message) {
                        this.toast.show(error.message, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else {
                        for(let key in error){
                            error[key].forEach(message => {
                                this.toast.show(message, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            });
                        }
                    }
                }
            );
    }

    chooseCategory(categoryId: number): void {
        this.choosedCategory = categoryId;
        this.page = 1;
        this.allCases = [];
        this.hasMore = true;
        let loader = this.loadingCtrl.create({
            content: Constants.LOADING_DATA_MESSAGE,
        });
        loader.present();
        this.getCatCases(loader, null);
    }

    loadMore(event: any): void {
        if(this.choosedCategory) {
            this.getCatCases(null, event);
        } else {
            this.getAllCases(null, event);
        }
    }

    sync(event: any) {
        this.page = 1;
        this.allCases = [];
        if(this.choosedCategory) {
            this.getCatCases(null, event);
        } else {
            this.getAllCases(null, event);
        }
    }

    getAllCases(loader: any, infiniteScroll: any): void {
        this.casesProvider.getAllCases(this.page)
            .subscribe(
                (response: any) => {
                    response.data.forEach(caseItem => {
                        this.allCases.push(caseItem);
                    });
                    this.page++;
                    if(this.page > response.last_page) {
                        this.hasMore = false;
                    }
                    if(loader) {
                        loader.dismiss();
                    }
                    if(infiniteScroll) {
                        infiniteScroll.complete();
                    }
                }, (error: any) => {
                    if(loader) {
                        loader.dismiss();
                    }
                    if(infiniteScroll) {
                        infiniteScroll.complete();
                    }
                    if(error.msg) {
                        this.toast.show(error.msg, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else if(error.message) {
                        this.toast.show(error.message, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else {
                        for(let key in error){
                            error[key].forEach(message => {
                                this.toast.show(message, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            });
                        }
                    }
                }
            );
    }

    getCatCases(loader: any, infiniteScroll: any): void {
        this.casesProvider.getCatCases(this.choosedCategory, this.page)
            .subscribe(
                (response: any) => {
                    response.data.forEach(caseItem => {
                        this.allCases.push(caseItem);
                    });
                    this.page++;
                    if(this.page > response.last_page) {
                        this.hasMore = false;
                    }
                    if(loader) {
                        loader.dismiss();
                    }
                    if(infiniteScroll) {
                        infiniteScroll.complete();
                    }
                }, (error: any) => {
                    if(loader) {
                        loader.dismiss();
                    }
                    if(infiniteScroll) {
                        infiniteScroll.complete();
                    }
                    if(error.msg) {
                        this.toast.show(error.msg, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else if(error.message) {
                        this.toast.show(error.message, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else {
                        for(let key in error){
                            error[key].forEach(message => {
                                this.toast.show(message, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            });
                        }
                    }
                }
            );
    }

    swipeEvent(e: any):void {
        if(e.direction == 4) {
            this.swipeProvider.swipe(this.navCtrl);
        }
    }
}
