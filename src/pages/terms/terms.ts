import {Component} from '@angular/core';
import {NavController, NavParams, LoadingController} from 'ionic-angular';
import {TermsProvider} from "../../providers/terms/terms";
import {Constants} from "../../shared/constants";
import {Toast} from "@ionic-native/toast";
import {SwipeProvider} from "../../providers/swipe/swipe";

@Component({
    selector: 'page-terms',
    templateUrl: 'terms.html',
})
export class TermsPage {

    public terms: any = [];

    constructor(public navCtrl: NavController, public navParams: NavParams, public swipeProvider: SwipeProvider, public termsProvider: TermsProvider, public loadingCtrl: LoadingController, private toast: Toast) {
    }

    ionViewDidLoad() {
        let loader = this.loadingCtrl.create({
            content: Constants.LOADING_DATA_MESSAGE,
        });
        loader.present();
        this.termsProvider.getTermsContent()
            .subscribe(
                (response: any) => {
                    this.terms = response;
                    loader.dismiss();
                }, (error: any) => {
                    loader.dismiss();
                    if(error.msg) {
                        this.toast.show(error.msg, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else if(error.message) {
                        this.toast.show(error.message, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else {
                        for(let key in error){
                            error[key].forEach(message => {
                                this.toast.show(message, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            });
                        }
                    }
                }
            );
    }

    swipeEvent(e: any):void {
        if(e.direction == 4) {
            this.swipeProvider.swipe(this.navCtrl);
        }
    }
}
