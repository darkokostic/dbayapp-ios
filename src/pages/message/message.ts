import {MyCasesPage} from './../my-cases/my-cases';
import {MyDonationsPage} from './../my-donations/my-donations';
import {MessagesPage} from './../messages/messages';
import {SettingsPage} from './../settings/settings';
import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {MessagesProvider} from "../../providers/messages/messages";
import {LocalStorageService} from "angular-2-local-storage";
import {Constants} from "../../shared/constants";
import {SwipeProvider} from "../../providers/swipe/swipe";

@Component({
    selector: 'page-message',
    templateUrl: 'message.html',
})
export class MessagePage {

    private message: any;
    public userInfo: any = {};

    constructor(public navCtrl: NavController, public swipeProvider: SwipeProvider, public navParams: NavParams, public messagesprovider: MessagesProvider, public localStorage: LocalStorageService) {
        this.message = this.navParams.data;
    }

    SettingsPage = SettingsPage;
    MessagesPage = MessagesPage;
    MyDonationsPage = MyDonationsPage;
    MyCasesPage = MyCasesPage;
    MessagePage = MessagePage;


    goToPage(pageName) {
        this.navCtrl.setRoot(pageName);
    }

    goBack() {
        this.navCtrl.pop();
    }

    ionViewDidEnter() {
        this.userInfo = this.localStorage.get(Constants.USER_INFO_LOCALSTORAGE);
    }

    swipeEvent(e: any):void {
        if(e.direction == 4) {
            this.swipeProvider.swipe(this.navCtrl);
        }
    }
}
