import {CaseCategoryPage} from './../case-category/case-category';
import {Component} from '@angular/core';
import {NavController, NavParams, LoadingController} from 'ionic-angular';
import {DonatePage} from "../donate/donate";
import {Constants} from "../../shared/constants";
import {LocalStorageService} from "angular-2-local-storage";
import {SwipeProvider} from "../../providers/swipe/swipe";

@Component({
    selector: 'page-donation-cats',
    templateUrl: 'donation-cats.html',
})
export class DonationCatsPage {
    private categories: any;
    private endpoint: string;
    constructor(public navCtrl: NavController, public navParams: NavParams, public swipeProvider: SwipeProvider, public loadingCtrl: LoadingController, public localStorage: LocalStorageService) {
        this.endpoint = Constants.IMAGES_ENDPOINT;
    }

    goBack() {
        this.navCtrl.pop();
    }

    goToCat(category): void {
        this.navCtrl.push(CaseCategoryPage, category);
    }

    goToDonate(): void {
        this.navCtrl.push(DonatePage, false);
    }

    ionViewDidEnter() {
        this.categories = this.localStorage.get(Constants.CATEGORY_LOCALSTORAGE);
    }

    swipeEvent(e: any):void {
        if(e.direction == 4) {
            this.swipeProvider.swipe(this.navCtrl);
        }
    }
}
