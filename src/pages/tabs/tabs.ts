import {CreateCasePage} from './../create-case/create-case';
import {MyDonationsPage} from './../my-donations/my-donations';
import {MessagesPage} from './../messages/messages';
import {SettingsPage} from './../settings/settings';
import {NewsFeedPage} from './../news-feed/news-feed';
import {MenuPage} from './../menu/menu';
import {MyCasesPage} from './../my-cases/my-cases';
import {AboutPage} from './../about/about';
import {DonationCatsPage} from './../donation-cats/donation-cats';
import {LoginPage} from './../login/login';
import {Component} from '@angular/core';
import {NavController, NavParams, LoadingController} from 'ionic-angular';
import {Constants} from "../../shared/constants";
import {CategoriesProvider} from "../../providers/categories/categories";
import {LocalStorageService} from "angular-2-local-storage";
import {Toast} from "@ionic-native/toast";

@Component({
    selector: 'page-tabs',
    templateUrl: 'tabs.html',

})


export class TabsPage {

    tab1Root = LoginPage;
    tab2Root = DonationCatsPage;
    tab3Root = AboutPage;
    myCasesPage = MyCasesPage;
    menuTap = MenuPage;
    newsFeed = NewsFeedPage;
    settingsPage = SettingsPage;
    messagesPage = MessagesPage;
    myDonationsPage = MyDonationsPage;
    CreateCasePage = CreateCasePage;


    constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, private categoriesProvider: CategoriesProvider, public localStorage: LocalStorageService, private toast: Toast) {

    }

    ionViewDidEnter() {
        this.categoriesProvider.getCategories()
            .subscribe(
                (response: any) => {
                    this.localStorage.set(Constants.CATEGORY_LOCALSTORAGE, response);
                }, (error: any) => {
                    if(error.msg) {
                        this.toast.show(error.msg, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else if(error.message) {
                        this.toast.show(error.message, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else {
                        for(let key in error){
                            error[key].forEach(message => {
                                this.toast.show(message, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            });
                        }
                    }
                }
            );
    }

}
