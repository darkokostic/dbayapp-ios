import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {AuthProvider} from "../../providers/auth/auth";
import {Constants} from "../../shared/constants";
import {Toast} from '@ionic-native/toast';
import {CitiesProvider} from "../../providers/cities/cities";
import {LocalStorageService} from "angular-2-local-storage";
import {TabsPage} from "../tabs/tabs";
import {LoadingController} from 'ionic-angular';
import {SwipeProvider} from "../../providers/swipe/swipe";

@Component({
    selector: 'page-register',
    templateUrl: 'register.html',
})
export class RegisterPage {

    tabBarElement: any;
    private form: FormGroup;
    private cities: any;
    private rulesText: any;
    private playerId: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public swipeProvider: SwipeProvider, public authProvider: AuthProvider, private toast: Toast, private citiesProvider: CitiesProvider, public localStorage: LocalStorageService, public loadingCtrl: LoadingController) {
        this.playerId = this.localStorage.get(Constants.ONESIGNAL_ID_KEY);
        this.form = new FormGroup({
            player_id: new FormControl(this.playerId),
            name: new FormControl('', Validators.required),
            email: new FormControl('', Validators.compose([
                Validators.required,
                Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
            password: new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(7)
            ])),
            password_confirmation: new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(7)
            ])),
            city: new FormControl('', Validators.required),
            area: new FormControl(''),
            street: new FormControl(''),
            phone: new FormControl('', Validators.required),
            rules: new FormControl(false, Validators.required)
        });
        this.tabBarElement = document.querySelector('.tabbar');
    }

    swipeEvent(e: any):void {
        if(e.direction == 4) {
            this.swipeProvider.swipe(this.navCtrl);
        }
    }

    ionViewWillEnter() {
        this.navCtrl.swipeBackEnabled = true;
        let loader = this.loadingCtrl.create({
            content: Constants.LOADING_DATA_MESSAGE,
        });
        loader.present();
        this.citiesProvider.getCities()
            .subscribe(
                (response: any) => {
                    this.cities = response;
                    this.form.controls.city.setValue(this.cities[0].id);
                    this.authProvider.getRules()
                        .subscribe(
                            (response: any) => {
                                this.rulesText = response.content1;
                                loader.dismiss();
                            }, (error: any) => {
                                loader.dismiss();
                                if(error.msg) {
                                    this.toast.show(error.msg, '2000', 'center')
                                        .subscribe(
                                            toast => {
                                                console.log(toast);
                                            }
                                        );
                                }
                                if(error.message) {
                                    this.toast.show(error.message, '2000', 'center')
                                        .subscribe(
                                            toast => {
                                                console.log(toast);
                                            }
                                        );
                                }
                            }
                        );
                }, (error: any) => {
                    loader.dismiss();
                    if(error.msg) {
                        this.toast.show(error.msg, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    }
                    if(error.message) {
                        this.toast.show(error.message, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    }
                }
            );
    }

    ionViewWillLeave() {
        this.navCtrl.swipeBackEnabled = true;
    }

    signUp(): void {
        if (this.form.valid) {
            if (this.form.value.password != this.form.value.password_confirmation) {
                this.toast.show(Constants.PASSWORDS_MUST_MATCH_MESSAGE, '2000', 'center')
                    .subscribe(
                        toast => {
                            console.log(toast);
                        }
                    );
            } else if(!this.form.value.rules) {
                this.toast.show(Constants.RULES_MESSAGE, '2000', 'center')
                    .subscribe(
                        toast => {
                            console.log(toast);
                        }
                    );
            } else {
                let loader = this.loadingCtrl.create({
                    content: Constants.PLEASE_WAIT_MESSAGE,
                });
                loader.present();
                this.authProvider.register(this.form.value)
                    .subscribe(
                        (response: any) => {
                            loader.dismiss();
                            if (!response.msg) {
                                this.localStorage.set(Constants.USER_INFO_LOCALSTORAGE, response.user_info[0]);
                                this.navCtrl.setRoot(TabsPage);
                                this.toast.show(Constants.REGISTER_MESSAGE, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            } else {
                                this.toast.show(response.msg, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            }
                        }, (error: any) => {
                            loader.dismiss();
                            if(error.msg) {
                                this.toast.show(error.msg, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            } else if(error.message) {
                                this.toast.show(error.message, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            } else {
                                for(let key in error){
                                    error[key].forEach(message => {
                                        this.toast.show(message, '2000', 'center')
                                            .subscribe(
                                                toast => {
                                                    console.log(toast);
                                                }
                                            );
                                    });
                                }
                            }
                        }
                    );
            }
        } else if (this.form.value.name == '' || this.form.value.email == '' || this.form.value.password == '' || this.form.value.password_confirmation == '' || this.form.value.city == '' || this.form.value.phone == '') {
            this.toast.show(Constants.FILL_ALL_FIELDS_MESSAGE, '2000', 'center')
                .subscribe(
                    toast => {
                        console.log(toast);
                    }
                );
        } else if (this.form.value.password.length < 7 || this.form.value.password_confirmation.length < 7) {
            this.toast.show(Constants.PASSWORD_LENGTH_MESSAGE, '2000', 'center')
                .subscribe(
                    toast => {
                        console.log(toast);
                    }
                );
        } else if(!this.form.value.rules) {
            this.toast.show(Constants.RULES_MESSAGE, '2000', 'center')
                .subscribe(
                    toast => {
                        console.log(toast);
                    }
                );
        } else {
            this.toast.show(Constants.EMAIL_FIELD_INVALID, '2000', 'center')
                .subscribe(
                    toast => {
                        console.log(toast);
                    }
                );
        }
    }

}
