import {Component} from '@angular/core';
import {NavController, NavParams, LoadingController} from 'ionic-angular';
import {AboutProvider} from "../../providers/about/about";
import {Constants} from "../../shared/constants";
import {Toast} from "@ionic-native/toast";
import {SwipeProvider} from "../../providers/swipe/swipe";

@Component({
    selector: 'page-about',
    templateUrl: 'about.html',
})
export class AboutPage {

    public about: any = [];

    constructor(public navCtrl: NavController, public navParams: NavParams, public aboutProvider: AboutProvider, public loadingCtrl: LoadingController, private toast: Toast, public swipeProvider: SwipeProvider) {
    }

    ionViewDidEnter() {
        let loader = this.loadingCtrl.create({
            content: Constants.LOADING_DATA_MESSAGE,
        });
        loader.present();
        this.aboutProvider.getAboutContent()
            .subscribe(
                (response: any) => {
                    this.about = response;
                    loader.dismiss();
                }, (error: any) => {
                    loader.dismiss();
                    if(error.msg) {
                        this.toast.show(error.msg, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else if(error.message) {
                        this.toast.show(error.message, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else {
                        for(let key in error){
                            error[key].forEach(message => {
                                this.toast.show(message, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            });
                        }
                    }
                }
            );
    }

    swipeEvent(e: any):void {
        if(e.direction == 4) {
            this.swipeProvider.swipe(this.navCtrl);
        }
    }
}
