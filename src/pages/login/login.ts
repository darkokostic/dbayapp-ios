import {ResetPasswordPage} from './../reset-password/reset-password';
import {RegisterPage} from './../register/register';
import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {FormGroup, FormControl, Validators} from "@angular/forms";
import {AuthProvider} from "../../providers/auth/auth";
import {Constants} from "../../shared/constants";
import {Toast} from '@ionic-native/toast';
import {TabsPage} from "../tabs/tabs";
import {LocalStorageService} from "angular-2-local-storage";
import {LoadingController} from 'ionic-angular';

@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {

    tabBarElement: any;
    private form: FormGroup;
    private playerId: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public authProvider: AuthProvider, private toast: Toast, public localStorage: LocalStorageService, public loadingCtrl: LoadingController) {
        this.tabBarElement = document.querySelector('.tabbar');
        this.playerId = this.localStorage.get(Constants.ONESIGNAL_ID_KEY);
        this.form = new FormGroup({
            player_id: new FormControl(this.playerId),
            email: new FormControl('', Validators.compose([
                Validators.required,
                Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
            ])),
            password: new FormControl('', Validators.required),
        });
    }

    login(): void {
        this.playerId = this.localStorage.get(Constants.ONESIGNAL_ID_KEY);
        this.form.controls['player_id'].setValue(this.playerId);
        if (this.form.valid) {
            let loader = this.loadingCtrl.create({
                content: Constants.PLEASE_WAIT_MESSAGE,
            });
            loader.present();
            this.authProvider.login(this.form.value)
                .subscribe(
                    (response: any) => {
                        loader.dismiss();
                        if(!response.msg) {
                            this.localStorage.set(Constants.USER_INFO_LOCALSTORAGE, response.user_info);
                            this.navCtrl.setRoot(TabsPage);
                            this.toast.show(Constants.LOGIN_MESSAGE, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        } else {
                            this.toast.show(response.msg, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        }
                    }, (error: any) => {
                        loader.dismiss();
                        if(error.msg) {
                            this.toast.show(error.msg, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        } else if(error.message) {
                            this.toast.show(error.message, '2000', 'center')
                                .subscribe(
                                    toast => {
                                        console.log(toast);
                                    }
                                );
                        } else {
                            for(let key in error){
                                error[key].forEach(message => {
                                    this.toast.show(message, '2000', 'center')
                                        .subscribe(
                                            toast => {
                                                console.log(toast);
                                            }
                                        );
                                });
                            }
                        }
                    }
                );
        } else if(this.form.value.email == '' || this.form.value.password == '') {
            this.toast.show(Constants.FILL_ALL_FIELDS_MESSAGE, '2000', 'center')
                .subscribe(
                    toast => {
                        console.log(toast);
                    }
                );
        } else {
            this.toast.show(Constants.EMAIL_FIELD_INVALID, '2000', 'center')
                .subscribe(
                    toast => {
                        console.log(toast);
                    }
                );
        }
    }


    register_page() {
        this.navCtrl.push(RegisterPage);
    }

    resetPassword() {
        this.navCtrl.push(ResetPasswordPage);
    }
}
