import {EditCasePage} from './../edit-case/edit-case';
import {MyDonationsPage} from './../my-donations/my-donations';
import {MessagesPage} from './../messages/messages';
import {SettingsPage} from './../settings/settings';
import {Component} from '@angular/core';
import {NavController, NavParams, LoadingController} from 'ionic-angular';
import {CasesProvider} from "../../providers/cases/cases";
import {Constants} from "../../shared/constants";
import {Toast} from "@ionic-native/toast";
import {LocalStorageService} from "angular-2-local-storage";
import {TabsTransitionProvider} from "../../providers/tabs-transition/tabs-transition";
import {SwipeProvider} from "../../providers/swipe/swipe";

@Component({
    selector: 'page-my-cases',
    templateUrl: 'my-cases.html',
})
export class MyCasesPage {

    private myCases: any = [];
    private message: string;
    private page: number;
    private hasMore: boolean;
    private userInfo: any = {};
    private credit: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public swipeProvider: SwipeProvider, private casesProvider: CasesProvider, public loadingCtrl: LoadingController, private toast: Toast, public localStorage: LocalStorageService, public tabsTransitionProvider: TabsTransitionProvider) {
        this.page = 1;
        this.hasMore = true;
    }

    SettingsPage = SettingsPage;
    MessagesPage = MessagesPage;
    MyDonationsPage = MyDonationsPage;
    MyCasesPage = MyCasesPage;
    EditCasePage = EditCasePage;

    goToPage(pageName) {
        this.navCtrl.setRoot(pageName);
    }

    editCase(caseObject) {
        this.page = 1;
        this.myCases = [];
        this.navCtrl.push(EditCasePage, caseObject);
    }

    ionViewDidEnter() {
        if(this.tabsTransitionProvider.getTab()) {
            this.navCtrl.push(this.tabsTransitionProvider.getTab());
            this.tabsTransitionProvider.setTab(null);
        } else {
            this.userInfo = this.localStorage.get(Constants.USER_INFO_LOCALSTORAGE);
            let loader = this.loadingCtrl.create({
                content: Constants.LOADING_DATA_MESSAGE,
            });
            loader.present();
            this.getMyCases(loader, null);
        }
    }

    loadMore(event: any): void {
        this.getMyCases(null, event);
    }

    sync(event: any) {
        this.page = 1;
        this.message = '';
        this.myCases = [];
        this.getMyCases(null, event);
    }

    closeCase(caseId): void {
        let loader = this.loadingCtrl.create({
            content: Constants.PLEASE_WAIT_MESSAGE,
        });
        loader.present();
        this.casesProvider.closeCase(caseId)
            .subscribe(
                (response: any) => {
                    loader.dismiss();
                    this.toast.show(Constants.CLOSED_CASE_MESSAGE, '2000', 'center')
                        .subscribe(
                            toast => {
                                console.log(toast);
                            }
                        );
                }, (error: any) => {
                    loader.dismiss();
                    if(error.msg) {
                        this.toast.show(error.msg, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else if(error.message) {
                        this.toast.show(error.message, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else {
                        for(let key in error){
                            error[key].forEach(message => {
                                this.toast.show(message, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            });
                        }
                    }
                }
            );
    }

    getMyCases(loader: any, infiniteScroll: any): void {
        this.casesProvider.getMyCases(this.page)
            .subscribe(
                (response: any) => {
                    response[1].data.forEach(caseItem => {
                        this.myCases.push(caseItem);
                    });
                    this.credit = response[0].credit;
                    this.message = Constants.MY_CASES_MESSAGE;
                    this.page++;
                    if(this.page > response[1].last_page) {
                        this.hasMore = false;
                    }
                    if(loader) {
                        loader.dismiss();
                    }
                    if(infiniteScroll) {
                        infiniteScroll.complete();
                    }
                }, (error: any) => {
                    if(loader) {
                        loader.dismiss();
                    }
                    if(infiniteScroll) {
                        infiniteScroll.complete();
                    }
                    if(error.msg) {
                        this.toast.show(error.msg, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else if(error.message) {
                        this.toast.show(error.message, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else {
                        for(let key in error){
                            error[key].forEach(message => {
                                this.toast.show(message, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            });
                        }
                    }
                }
            );
    }

    swipeEvent(e: any):void {
        if(e.direction == 4) {
            this.swipeProvider.swipe(this.navCtrl);
        }
    }
}
