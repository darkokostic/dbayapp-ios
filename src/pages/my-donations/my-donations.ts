import {MyCasesPage} from './../my-cases/my-cases';
import {MessagesPage} from './../messages/messages';
import {SettingsPage} from './../settings/settings';
import {Component} from '@angular/core';
import {NavController, NavParams, LoadingController} from 'ionic-angular';
import {DonationsProvider} from "../../providers/donations/donations";
import {Constants} from "../../shared/constants";
import {LocalStorageService} from "angular-2-local-storage";
import {Toast} from "@ionic-native/toast";
import {TabsTransitionProvider} from "../../providers/tabs-transition/tabs-transition";
import {SwipeProvider} from "../../providers/swipe/swipe";

@Component({
    selector: 'page-my-donations',
    templateUrl: 'my-donations.html',
})
export class MyDonationsPage {

    private myDonations: any = [];
    private message: string;
    private page: number;
    private hasMore: boolean;
    public userInfo: any = {};

    constructor(public navCtrl: NavController, public navParams: NavParams, public swipeProvider: SwipeProvider, private donationsProvider: DonationsProvider, public loadingCtrl: LoadingController, public localStorage: LocalStorageService, private toast: Toast, public tabsTransitionProvider: TabsTransitionProvider) {
        this.page = 1;
        this.hasMore = true;
    }

    SettingsPage = SettingsPage;
    MessagesPage = MessagesPage;
    MyDonationsPage = MyDonationsPage;
    MyCasesPage = MyCasesPage;

    goToPage(pageName) {
        this.navCtrl.setRoot(pageName);
    }

    swipeEvent(e: any):void {
        if(e.direction == 4) {
            this.swipeProvider.swipe(this.navCtrl);
        }
    }

    ionViewDidEnter() {
        if(this.tabsTransitionProvider.getTab()) {
            this.navCtrl.push(this.tabsTransitionProvider.getTab());
            this.tabsTransitionProvider.setTab(null);
        } else {
            this.userInfo = this.localStorage.get(Constants.USER_INFO_LOCALSTORAGE);
            let loader = this.loadingCtrl.create({
                content: Constants.LOADING_DATA_MESSAGE,
            });
            loader.present();
            this.getMyDonations(loader, null);
        }
    }

    loadMore(event: any): void {
        this.getMyDonations(null, event);
    }

    sync(event: any) {
        this.page = 1;
        this.message = '';
        this.myDonations = [];
        this.getMyDonations(null, event);
    }

    getMyDonations(loader: any, infiniteScroll: any): void {
        this.donationsProvider.getMyDonations(this.page)
            .subscribe(
                (response: any) => {
                    response.data.forEach(caseItem => {
                        this.myDonations.push(caseItem);
                    });
                    this.message = Constants.MY_DONATIONS_MESSAGE;
                    this.page++;
                    if(this.page > response.last_page) {
                        this.hasMore = false;
                    }
                    if(loader) {
                        loader.dismiss();
                    }
                    if(infiniteScroll) {
                        infiniteScroll.complete();
                    }
                }, (error: any) => {
                    if(loader) {
                        loader.dismiss();
                    }
                    if(infiniteScroll) {
                        infiniteScroll.complete();
                    }
                    if(error.msg) {
                        this.toast.show(error.msg, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else if(error.message) {
                        this.toast.show(error.message, '2000', 'center')
                            .subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                    } else {
                        for(let key in error){
                            error[key].forEach(message => {
                                this.toast.show(message, '2000', 'center')
                                    .subscribe(
                                        toast => {
                                            console.log(toast);
                                        }
                                    );
                            });
                        }
                    }
                }
            );
    }
}
