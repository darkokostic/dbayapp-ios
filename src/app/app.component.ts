import {Component, ViewChild} from '@angular/core';
import {Platform, NavController} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {LoginPage} from "../pages/login/login";
import {LocalStorageService} from "angular-2-local-storage";
import {Constants} from "../shared/constants";
import {TabsPage} from "../pages/tabs/tabs";
@Component({
    templateUrl: 'app.html'
})

// set status bar to white

export class MyApp {


    rootPage: any;

    @ViewChild('content') nav: NavController;

    goToPage(pageName) {
        this.nav.setRoot(pageName);
    }

    constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public localStorage: LocalStorageService) {
        let self = this;
        platform.ready().then(() => {
            // this.oneSignal.startInit('559b5c45-3f62-48f6-bf35-45d03ca8b3db', '915828085143');
            //
            // this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
            //
            // this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
            //
            // this.oneSignal.handleNotificationReceived().subscribe((response) => {
            //     console.log(response);
            // });
            //
            // this.oneSignal.handleNotificationOpened().subscribe((response) => {
            //     console.log(response);
            // });
            //
            // this.oneSignal.getIds()
            //     .then((response: any) => {
            //         this.localStorage.set(Constants.ONESIGNAL_ID_KEY, response.userId);
            //     });
            //
            // this.oneSignal.endInit();

            if(!this.localStorage.get(Constants.USER_INFO_LOCALSTORAGE)) {
                self.rootPage = LoginPage;
            } else {
                self.rootPage = TabsPage;
            }
            statusBar.styleDefault();
            setTimeout(() => {
                splashScreen.hide();
            }, 500);
        });
    }

}

