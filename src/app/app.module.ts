import {FaqPage} from '../pages/faq/faq';
import {ContactPage} from '../pages/contact/contact';
import {TermsPage} from '../pages/terms/terms';
import {ResetPasswordPage} from '../pages/reset-password/reset-password';
import {DonatePage} from '../pages/donate/donate';
import {RegisterPage} from '../pages/register/register';
import {CaseCategoryPage} from '../pages/case-category/case-category';
import {EditCasePage} from '../pages/edit-case/edit-case';
import {CreateCasePage} from '../pages/create-case/create-case';
import {MessagePage} from '../pages/message/message';
import {MyDonationsPage} from '../pages/my-donations/my-donations';
import {MessagesPage} from '../pages/messages/messages';
import {SettingsPage} from '../pages/settings/settings';
import {CasePage} from '../pages/case/case';
import {NewsFeedPage} from '../pages/news-feed/news-feed';
import {MenuPage} from '../pages/menu/menu';
import {MyCasesPage} from '../pages/my-cases/my-cases';
import {AboutPage} from '../pages/about/about';
import {TabsPage} from '../pages/tabs/tabs';
import {DonationCatsPage} from '../pages/donation-cats/donation-cats';
import {LoginPage} from '../pages/login/login';
import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';
import {Camera} from '@ionic-native/camera';
import {FileOpener} from '@ionic-native/file-opener';
import {IonicImageViewerModule} from 'ionic-img-viewer';
import {MyApp} from './app.component';
import {Constants} from "../shared/constants";
import {XHRBackend, RequestOptions, Http, HttpModule} from "@angular/http";
import {LocalStorageModule, LocalStorageService} from "angular-2-local-storage";
import {HttpInterceptor} from "../interceptors/http.interceptor";
import {CasesProvider} from '../providers/cases/cases';
import {AboutProvider} from '../providers/about/about';
import {FaqProvider} from '../providers/faq/faq';
import {DonationsProvider} from '../providers/donations/donations';
import {AuthProvider} from '../providers/auth/auth';
import {TermsProvider} from '../providers/terms/terms';
import {ContactProvider} from '../providers/contact/contact';
import {SettingsProvider} from '../providers/settings/settings';
import {Toast} from "@ionic-native/toast";
import {CitiesProvider} from '../providers/cities/cities';
import {LimitToPipe} from '../pipes/limit-to/limit-to';
import { MessagesProvider } from '../providers/messages/messages';
import { CategoriesProvider } from '../providers/categories/categories';
import { ImagesProvider } from '../providers/images/images';
import {Keyboard} from "@ionic-native/keyboard";
import { TabsTransitionProvider } from '../providers/tabs-transition/tabs-transition';
import { SwipeProvider } from '../providers/swipe/swipe';

export function httpInterceptor(backend: XHRBackend, defaultOptions: RequestOptions, localStorage: LocalStorageService) {
    return new HttpInterceptor(backend, defaultOptions, localStorage);
}

@NgModule({
    declarations: [
        MyApp,
        LoginPage,
        RegisterPage,
        DonationCatsPage,
        TabsPage,
        AboutPage,
        MyCasesPage,
        MyDonationsPage,
        MenuPage,
        NewsFeedPage,
        CasePage,
        SettingsPage,
        MessagesPage,
        MessagePage,
        CreateCasePage,
        EditCasePage,
        CaseCategoryPage,
        DonatePage,
        ResetPasswordPage,
        TermsPage,
        ContactPage,
        FaqPage,
        LimitToPipe,

    ],
    imports: [
        HttpModule,
        LocalStorageModule.withConfig({
            storageType: 'localStorage'
        }),
        BrowserModule,
        IonicImageViewerModule,
        IonicModule.forRoot(MyApp, {
            tabsHideOnSubPages: false,
            swipeBackEnabled: true,
            modalEnter: 'modal-slide-in',
            modalLeave: 'modal-slide-out',
        })
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        LoginPage,
        RegisterPage,
        DonationCatsPage,
        TabsPage,
        AboutPage,
        MyCasesPage,
        MyDonationsPage,
        MenuPage,
        NewsFeedPage,
        CasePage,
        SettingsPage,
        MessagesPage,
        MessagePage,
        CreateCasePage,
        EditCasePage,
        CaseCategoryPage,
        DonatePage,
        ResetPasswordPage,
        TermsPage,
        ContactPage,
        FaqPage,
    ],
    providers: [
        StatusBar,
        SplashScreen,
        Constants,
        Camera,
        FileOpener,
        CasesProvider,
        AboutProvider,
        FaqProvider,
        DonationsProvider,
        AuthProvider,
        TermsProvider,
        ContactProvider,
        SettingsProvider,
        Toast,
        Keyboard,
        {
            provide: Http,
            useFactory: httpInterceptor,
            deps: [XHRBackend, RequestOptions, LocalStorageService]
        },
        {
            provide: ErrorHandler,
            useClass: IonicErrorHandler
        },
        CitiesProvider,
    MessagesProvider,
    CategoriesProvider,
    ImagesProvider,
    TabsTransitionProvider,
    SwipeProvider,
    ]
})
export class AppModule {
}
